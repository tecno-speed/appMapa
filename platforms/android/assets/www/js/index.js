var map;

//https://github.com/mapsplugin/cordova-plugin-googlemaps

document.addEventListener('deviceready', onDeviceready)

function onDeviceready() {
    var divMapa = document.getElementById('mapa')
    //map = plugin.google.maps.Map.getMap(divMapa)

}

function capturarGeo() {
    var options = {
        enableHighAccurancy: true
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
}

function onSuccess(position) {
    var msg = "Lat: " + position.coords.latitude + " - Log: " + position.coords.longitude
    document.getElementById('retornoGeo').innerHTML = msg;

    var localizacao = {
        lat: position.coords.latitude,
        lon: position.coords.longitude,
        lng: position.coords.longitude
    }

    var mapX = new google.maps.Map(document.getElementById('mapa2'), {
        zoom: 4,
        center: localizacao
    });

    var marker = new google.maps.Marker({
        position: localizacao,
        map: mapX
    });

    // Move to the position with animation
    // map.animateCamera({
    //     target: localizacao,
    //     zoom: 17,
    //     tilt: 60,
    //     bearing: 140,
    //     duration: 5000
    // }, function () {

    //     // Add a maker
    //     map.addMarker({
    //         position: localizacao,
    //         title: "Estou aqui",
    //         snippet: "Que legal!",
    //         animation: plugin.google.maps.Animation.BOUNCE
    //     }, function (marker) {

    //         // Show the info window
    //         marker.showInfoWindow();
    //     });
    // });
}

function onError(error) {
    alert(error.message)
}